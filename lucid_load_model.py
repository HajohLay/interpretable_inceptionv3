from lucid.modelzoo.vision_base import Model, _layers_from_list_of_dicts

class InceptionV3Lucid(Model):
    model_path = '/content/interpretable_inceptionv3/lucid_model/lucid_inception_v3_colab.pb'
    image_shape = [299,299,3]
    image_value_range = [-1,1]
    input_name = "input_1"
