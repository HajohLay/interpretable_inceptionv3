import numpy as np
import tensorflow as tf

if __name__ == '__main__':
    sess = tf.Session()
    tf.keras.backend.set_session(sess)
    tf.keras.models.load_model("keras_models/inception_v3.h5")


    layerfile = open("layers/layers.txt", "w")
    for op in tf.compat.v1.get_default_graph().get_operations():
        print(op.name)
        layerfile.write(op.name + "\n")