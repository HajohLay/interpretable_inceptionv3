import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
import os

from lucid.modelzoo.vision_models import Model


model_name = "inception_v3.h5"
input_name = "input_2"
out_names = ["predictions/Softmax"]
image_size = [299,299,3]
image_value_range = (0,255)


with K.get_session().as_default():

#     # model = tf.keras.models.load_model("keras_models/dense_frozen.h5", custom_objects={"Functional":tf.keras.models.Model})
    model = tf.keras.models.load_model("keras_models/inception_v3.h5", )
    Model.save("lucid_models/inception_v3.pb",input_name, out_names,image_size, image_value_range)

    # Model.suggest_save_args()
