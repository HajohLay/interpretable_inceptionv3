from matplotlib import image
import tensorflow as tf
import numpy as np
import cv2
import scipy as sp
import math

from tensorflow.keras.applications.inception_v3 import preprocess_input, decode_predictions

def load_img(image_path):
    img = cv2.imread(image_path)
    rgb_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return preprocess_input(rgb_img)

def load_raw_image(image_path):
    img = cv2.imread(image_path)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# def load_cropped_image(image_path):
#     img = cv2.imread(image_path)
#     #state crop and original dims
#     crop_width = 299
#     crop_height = 299
#     width, height = img.shape[1], img.shape[0]

#     aspect_ratio = width/height
#     if (width < height):
#         resize_width = 299
#         resize_height = int(resize_width / aspect_ratio)
#     else:
#         resize_height = 299
#         resize_width = int(resize_height * aspect_ratio)
#     resize_img = cv2.resize(img, (resize_width+1, resize_height+1), interpolation=cv2.INTER_AREA)
    
#     #determine center of original image half crop width / height
#     mid_x, mid_y = int(resize_width/2) + 1, int(resize_height/2) + 1
#     cw2, ch2 = int(crop_width/2), int(crop_height/2)
    
#     crop_img = resize_img[mid_y-ch2:mid_y+ch2+1, mid_x-cw2:mid_x+cw2+1]
#     return crop_img

 

class InceptionV3():

    def __init__(self, model_path="content/interpretable_incpetionv3_dataset/models/inception_v3_colab.h5", target_name = 'mixed9/concat:0'):
        sess = tf.compat.v1.Session()
        tf.compat.v1.keras.backend.set_session(sess)
        init_op = tf.compat.v1.global_variables_initializer()
        sess.run(init_op)
        self.sess = sess
        self.graph = sess.graph
        self.model = tf.keras.models.load_model(model_path)

        #tensors
        self.image_tensor = self.graph.get_tensor_by_name("input_1:0")
        self.target_tensor = self.graph.get_tensor_by_name(target_name)
        self.out_tensor = self.graph.get_tensor_by_name('predictions/Softmax:0')
    
    def summary(self):
        self.model.summary()
    
    def get_raw_prediction(self, image_dir):
        img = load_img(image_dir)
        return self.model.predict([[img]])

    def get_prediction(self, image_dir):
        return decode_predictions(self.get_raw_prediction(image_dir))
    
    def get_activations(self, image_dir, target_name='mixed9/concat:0'):
        img = load_img(image_dir)
        activations = self.sess.run([self.target_tensor], feed_dict={self.image_tensor:[img]})
        max_activation = np.max(activations)
        return activations[0][0], max_activation
    
    def get_activations_for_dict(self, image_dir, target_name='mixed9/concat:0'):
        acts, max_act= self.get_activations(image_dir, target_name=target_name)
        cropped_acts = [[[{"n": n.tolist(), "v": float(act_vec[n])} for n in np.flip(np.argsort(act_vec)[-2:])] for act_vec in act_slice] for act_slice in acts]
        return cropped_acts, max_act
        
    def get_scores(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
        img = load_img(image_dir)

        #compute grads
        one_hot = tf.sparse_to_dense(class_index, [1000], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]
        
        #compute scores
        output, grads_val = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:[img]})
        class_score = np.multiply(output, grads_val)[0]
        return class_score, np.max(class_score)
    
    def get_scores_for_dict(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
        scores, max_score = self.get_scores(image_dir, class_index=class_index, target_name=target_name)
        cropped_scores = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-2:])] for score_vec in score_slice] for score_slice in scores]
        return cropped_scores, max_score

    def get_smooth_scores(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
        interpolated_images = self.interpolate_images(image_dir)

        #compute grads
        one_hot = tf.sparse_to_dense(class_index, [1000], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]

        #output scores
        acts, grads_vals = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:interpolated_images})

        grads_by_vals = np.multiply(acts, grads_vals)
        smooth_scores = np.mean(grads_by_vals, axis=0)
        return smooth_scores, np.max(smooth_scores)
    
    def get_smooth_score_for_dict(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
        scores, max_score = self.get_smooth_scores(image_dir, class_index=class_index, target_name=target_name)
        cropped_scores = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-2:])] for score_vec in score_slice] for score_slice in scores]
        return cropped_scores, max_score 
    
    def interpolate_images(self, image_dir, no_interpolations = 5):
        img = load_img(image_dir)
        interpolated_images = []
        for i in range(1,no_interpolations+1):
            interpolated_images.append(img * (i/(no_interpolations)))
        return interpolated_images
    
    def get_integrated_image_gradients(self, image_dir, class_index = 0, no_interpolations = 5,):
        interpolated_images = self.interpolate_images(image_dir, no_interpolations=no_interpolations)
        #get activations for batch 
        one_hot = tf.sparse_to_dense(class_index, [1000], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.image_tensor)[0]
        grads_val = self.sess.run([grads], feed_dict={self.image_tensor:interpolated_images})

        grads_by_vals = np.multiply(interpolated_images, grads_val)
        weights = np.mean(grads_by_vals[0], axis=0)
        reduce_color_axis = np.mean(weights, axis=-1)
        truncated = np.maximum(reduce_color_axis, 0)
        normalized = truncated / np.amax(truncated)
        shape = np.shape(normalized)
        overlay = cv2.cvtColor(cv2.applyColorMap(np.uint8(255*normalized), cv2.COLORMAP_MAGMA), cv2.COLOR_BGR2RGB)

        #generate image
        raw_image = load_raw_image(image_dir)
        return raw_image, overlay, overlay*0.6 + raw_image * 0.4