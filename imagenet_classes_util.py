import json

class ImagenetClassesUtil():
    
    def __init__(self, class_file_path):
        name_index = 1
        code_index = 0
        self.code_to_index = dict()
        self.index_to_code = dict()
        self.index_to_name = dict()
        self.name_to_index = dict()

        f = open(class_file_path)
        json_data = json.load(f)
        for index in json_data.keys():
            int_index = int(index)
            name = json_data[index][name_index]
            code = json_data[index][code_index]
            self.code_to_index[code] = int_index
            self.index_to_code[int_index] = code
            self.index_to_name[int_index] = name
            self.name_to_index[name] = int_index
    
    def get_name_from_index(self, index):
        return self.index_to_name[index]
    
    def get_index_from_name(self, name):
        return self.name_to_index[name]
    
    def get_code_from_index(self, index):
        return self.index_to_code[index]
    
    def get_index_from_code(self, code):
        return self.code_to_index[code]

    def get_name_from_code(self, code):
        return self.index_to_name[self.code_to_index[code]]
    
    def get_code_from_name(self, name):
        return self.index_to_code[self.name_to_index[name]]
