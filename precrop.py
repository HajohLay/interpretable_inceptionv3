import cv2, os
import numpy as np
import inception_v3

if __name__ == "__main__":
    done = 0
    todo = len(os.listdir("dataset/imagenet"))
    for imagename in os.listdir("dataset/imagenet"):
        cropped = inception_v3.load_cropped_image("dataset/imagenet/" + imagename)
        if (len(cropped) != 299 or len(cropped[0]) != 299):
            continue
        cv2.imwrite("dataset/precropped/" + imagename, cropped)
        done = done + 1
        # print(np.shape(cropped))
        if (done % 100 == 0):
            print(f"done {done} of {todo}")
